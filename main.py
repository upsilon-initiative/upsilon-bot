"""Upsilon discord bot"""
# pylint: disable=logging-format-interpolation
import asyncio
import json
import logging
import multiprocessing.connection
import os
import re
import shutil

import discord
import sqlalchemy
import sqlalchemy.orm
import sqlalchemy.ext.declarative

from db_relations import Submission, User


## setup
# config loading
CONFIG = json.load(open("config.json"))
SECRETS = json.load(open("secrets.json"))

# database setup
Engine = sqlalchemy.create_engine(SECRETS["database_connect_string"], future=True)
SessionMaker = sqlalchemy.orm.sessionmaker(bind=Engine, future=True)


# logging
def create_sublogger(level, path):
    """Sets up a sublogger"""
    formatter = logging.Formatter(
        "%(asctime)s %(name)s %(process)d %(levelname)s %(message)s"
    )
    logger_handler = logging.FileHandler(path)
    logger_handler.setLevel(level)
    logger_handler.setFormatter(formatter)
    return logger_handler


ROOT_LOGGER = logging.getLogger()
ROOT_LOGGER.setLevel(logging.INFO)
os.makedirs("logs", exist_ok=True)
ROOT_LOGGER.addHandler(create_sublogger(logging.DEBUG, "logs/upsilon_bot.log"))
LOGGER = logging.getLogger("upsilon_bot")

# discord channels
CHANNELS = dict(default=None, submission=None)
DISCORD_DATA = dict(channels=CHANNELS, guild=None)

# pending craft confirms
PENDING_CONFIRMS = {}

# gateway intents
INTENTS = discord.Intents.default()
INTENTS.members = True
INTENTS.typing = False
INTENTS.presences = False

# discord client
CLIENT = discord.Client(intents=INTENTS)

# constants
BOT_COLOR = 0x4E5D94
TIMEOUT = 60


## helpers


def get_submission_paths(submission_id, craft_id=None):
    base = os.path.join(
        CONFIG["data_storage_path"], CONFIG["submissions"], str(submission_id)
    )
    out = {"base": base, "images": os.path.join(base, CONFIG["images_path"])}
    if craft_id is not None:
        out["craft"] = os.path.join(base, "{0}.json".format(craft_id))
    return out


def check_if_admin(user):
    """Checks if a given user qualifies as an admin based on their roles"""
    admin_ids = set(CONFIG["admin_role_ids"])
    user_roles = set(r.id for r in user.roles)
    return bool(admin_ids.intersection(user_roles))


async def await_confirmation(submission_id):
    """Removes a craft from PENDING_CONFIRMS after TIMEOUT has elapsed"""
    await asyncio.sleep(TIMEOUT)
    if PENDING_CONFIRMS.pop(submission_id, None) is not None:
        await CHANNELS["default"].send(
            "Operation timed out for {0}".format(submission_id)
        )


async def delete_message(message_id):
    """Delets a message given the message id"""
    try:
        message = await CHANNELS["submission"].fetch_message(message_id)
    except discord.errors.NotFound:
        LOGGER.warning("Attempted to delete non-existent message")
    else:
        await message.delete()


def handle_reaction(payload, change):
    """Handles reaction management for voting"""
    session = SessionMaker()
    emojis = CONFIG["emojis"]
    # only handle if in submission channel, not authored by bot and emoji valid
    if (
        payload.channel_id != CHANNELS["submission"].id
        or payload.user_id == CLIENT.user.id
        or payload.emoji.name not in emojis
    ):
        return
    submission = session.execute(
        sqlalchemy.select(Submission).filter_by(message_id=payload.message_id)
    ).scalar_one_or_none()
    if submission is None:
        return
    if payload.emoji.name == emojis[0]:
        submission.positive_votes += change
    elif payload.emoji.name == emojis[1]:
        submission.negative_votes += change
    submission.approved = (
        submission.positive_votes - submission.negative_votes * 3 >= 10
    )
    session.commit()
    session.close()


def verify_submission_args(message_content):
    content = message_content.split()
    if len(content) == 1:
        return "You must specify a submission ID"
    if len(content) > 2:
        return "Only one submission ID can be specified"
    try:
        return int(content[1])
    except ValueError:
        return "Invalid submission ID: {0}".format(content[1])


## usage funcs


async def admin_operation(message, session, state, action_description):
    """Handles confirmed admin operations"""
    # checks if the user has the rights to use this command
    if not check_if_admin(message.author):
        return "You don't have permission to call this command"
    # checks for the submission
    result = verify_submission_args(message.content)
    if isinstance(result, str):
        return result
    submission_id = result
    submission = session.execute(
        sqlalchemy.select(Submission).filter_by(submission_id=submission_id)
    ).scalar_one_or_none()
    if submission is None:
        return "There is no such submission: {0}".format(submission_id)
    # starts confirm timer
    PENDING_CONFIRMS[submission_id] = state
    CLIENT.loop.create_task(await_confirmation(submission_id))

    return (
        f"Are you sure you want to {action_description} {submission_id}? "
        f" Type `%confirm {submission_id}` within the next {TIMEOUT}s to confirm"
    )


# bot commands
async def help_(*_):
    """Generates help embed"""
    embed = discord.Embed(title="Bot Help", color=BOT_COLOR)
    for name, command_details in COMMANDS.items():
        embed.add_field(
            name="%{0}".format(name), value=command_details["description"], inline=True
        )
    return embed


async def my_submissions(message, session):
    """Generates user submissions embed"""
    # fetches all submissions from message author
    user_id = message.author.id
    user = session.execute(
        sqlalchemy.select(User).filter_by(discord_id=user_id)
    ).scalar_one_or_none()

    if (not user) or (not user.submission):
        description = "You haven't submit anything yet"
    else:
        # creates an embed containing all the crafts
        description = ", ".join(
            [
                ", ".join([craft.name for craft in submission.craft])
                for submission in user.submission
            ]
        )
    return discord.Embed(
        title=f"Crafts submitted from {message.author.name}",
        color=BOT_COLOR,
        description=description,
    )


async def show_rules(*_):
    """Generates rules embed"""
    embed = discord.Embed(title="Rules", color=BOT_COLOR)
    embed.set_author(name="Upsilon senior executive management board")
    for index, rule in enumerate(CONFIG["rules"]):
        embed.add_field(name="Rule №{:>2d}".format(index + 1), value=rule, inline=False)
    embed.description = CONFIG["rules_general"]
    embed.set_footer(
        text='For further information ask one of the moderators ("{0}" role)'.format(
            CONFIG["moderator_role_name"]
        )
    )
    return embed


async def force_approve(message, session):
    """Force approve command"""
    return await admin_operation(message, session, "approve", "force approve")


async def force_delete(message, session):
    """Force delete command"""
    return await admin_operation(message, session, "delete", "delete")


async def confirm(message, session):
    """Confirms the approval or deletion of a craft """
    # checks if the user has the rights to use this command
    if not check_if_admin(message.author):
        return "You don't have the permission to call this command"

    # checks for the submission
    result = verify_submission_args(message.content)
    if isinstance(result, str):
        return result
    submission_id = result

    confirm_state = PENDING_CONFIRMS.pop(submission_id, None)
    if confirm_state is None:
        return "No pending confirmations for this submission"
    submission = session.execute(
        sqlalchemy.select(Submission).filter_by(submission_id=submission_id)
    ).scalar_one_or_none()
    if submission is None:
        return "Submission no longer exists"
    if confirm_state == "approve":
        submission.approved = True
        session.commit()
        return "Successfully approved {0}".format(submission_id)
    if confirm_state == "delete":
        files_path = get_submission_paths(submission.submission_id)["base"]
        shutil.rmtree(files_path)
        message_id = submission.message_id
        session.delete(submission)
        session.commit()
        await delete_message(message_id)
        return "Successfully deleted {0}".format(submission_id)


async def pro_gamers(message, _):
    if not check_if_admin(message.author):
        return "You don't have the permission to call this command"
    roles = set(CONFIG["role_names"].values())
    complete_usernames = []
    async for member in DISCORD_DATA["guild"].fetch_members(limit=None):
        if set(role.name for role in member.roles) >= roles:
            complete_usernames.append(str(member))
    embed = discord.Embed(title="Pro gamers™", color=0x9900FF)
    embed.add_field(
        name="The elite few who have submitted to every segment",
        value="\n".join(complete_usernames),
    )
    return embed


## client events


@CLIENT.event
async def on_ready():
    """Gets called when the discord client is ready """
    guild = CLIENT.get_guild(CONFIG["guild"])
    for channel in CHANNELS:
        CHANNELS[channel] = discord.utils.get(
            guild.channels, name=CONFIG["channels"][channel]
        )
    DISCORD_DATA["guild"] = guild
    LOGGER.info("Bot initialised")


@CLIENT.event
async def on_message(message):
    """Handles all user interaction with bot"""
    if (
        not message.author.bot
        and message.content.startswith(CONFIG["prefix"])
        and message.channel == CHANNELS["default"]
    ):
        command_name = message.content.split()[0][len(CONFIG["prefix"]) :]
        if command_name not in COMMANDS:
            output = "{0} is not a valid command".format(message.content.split()[0])
        else:
            # call response function for command
            try:
                func = COMMANDS[command_name]["function"]
                with SessionMaker.begin() as session:
                    output = await func(message, session)
            except Exception:
                LOGGER.exception("Error in {0}".format(command_name))
                output = "<@&{0}> An error occured processing this command".format(
                    CONFIG["bot_error_role"]
                )

        # post the message
        if isinstance(output, str):
            await CHANNELS["default"].send(output)
        else:
            await CHANNELS["default"].send(" ", embed=output)


@CLIENT.event
async def on_error(event, *args, **kwargs):
    """Logs uncaught exceptions to the logfile"""
    # pylint: disable=unused-argument
    LOGGER.exception("Uncaught exception")


@CLIENT.event
async def on_raw_reaction_add(payload):
    """Handles added reactions"""
    handle_reaction(payload, 1)


@CLIENT.event
async def on_raw_reaction_remove(payload):
    """Handles removed reactions"""
    handle_reaction(payload, -1)


## multiprocessing link


async def handle_multiprocessing_message(message):
    """Handles messages recieved from the site"""
    action = message["action"]
    if action == "post_submission":
        submission_id = message["submission_id"]
        session = SessionMaker()
        submission = session.execute(
            sqlalchemy.select(Submission).filter_by(submission_id=submission_id)
        ).scalar_one()
        try:
            discord_user = await DISCORD_DATA["guild"].fetch_member(
                submission.user.discord_id
            )
        except Exception:
            LOGGER.exception(
                "Error getting member {0}".format(submission.user.discord_id)
            )
            discord_user = None
        if discord_user is None:
            discord_user = CLIENT.user
            user_name = "Anonymous"
        else:
            user_name = discord_user.name

        craft = submission.craft[0]
        situation = "{0}: {1}".format(craft.parent, craft.situation.capitalize())

        embed = discord.Embed(
            title=craft.categorisation,
            color=BOT_COLOR,
            description=f"{discord_user.mention} uploaded a new submission: {craft.name}",
        )
        embed.set_author(name=user_name, url=discord_user.avatar_url)
        embed.add_field(name="Situation", value=situation)
        embed.add_field(
            name="Check out the submission!",
            value="{0}/submissions/{1}/".format(
                CONFIG["site_link"], submission.submission_id
            ),
        )

        # send the message and setup voting
        embed_message = await CHANNELS["submission"].send(" ", embed=embed)
        for emojiname in CONFIG["emojis"]:
            await embed_message.add_reaction(
                discord.utils.get(CLIENT.emojis, name=emojiname)
            )

        # update database with message id
        submission.message_id = embed_message.id

        # give role
        if discord_user is not CLIENT.user:
            parent = craft.parent
            moon_lookup = CONFIG["parent_lookups"].get(parent, None)
            if moon_lookup is not None:
                parent = moon_lookup
            role_name = CONFIG["role_names"][parent]
            appropriate_role = discord.utils.get(
                DISCORD_DATA["guild"].roles, name=role_name
            )
            member = DISCORD_DATA["guild"].get_member(discord_user.id)
            if member is None:
                LOGGER.warning(
                    "Attempted to add role to <{0}>, user not present".format(
                        discord_user.id
                    )
                )
            else:
                await member.add_roles(
                    appropriate_role,
                    reason="Assigned for a {0} submission".format(craft.parent),
                )
        session.commit()
        session.close()

    elif action == "delete_submission":
        message_id = message["message_id"]
        await delete_message(message_id)

    elif action == "rename_submission":
        message_id = message["message_id"]
        try:
            submission_message = await CHANNELS["submission"].fetch_message(message_id)
        except discord.errors.NotFound:
            LOGGER.error("Attempted to rename non-existent submission")
        else:
            embed = submission_message.embeds[0]
            embed.description = re.sub(
                r"submission: .+$",
                "submission: {0}".format(message["name"]),
                embed.description,
            )
            await submission_message.edit(embed=embed, suppress=False)


async def multiprocessing_link():
    """Multiprocessing IPC link to upsilon site"""
    address = CONFIG["socket_location"]
    while True:
        listener = multiprocessing.connection.Listener(
            address, family="AF_UNIX", authkey=SECRETS["bot_socket"].encode()
        )
        LOGGER.info("Waiting for upsilon IPC to connect")
        connection = listener.accept()
        LOGGER.info("Upsilon IPC connected")
        while True:
            if connection.poll():
                try:
                    message = connection.recv()
                except EOFError:
                    LOGGER.error("Upsilon IPC connection unexpectedly closed")
                    listener.close()
                    break
                else:
                    if CHANNELS["submission"] is None:
                        LOGGER.warning("IPC message waiting for channel setup")
                        while CHANNELS["submission"] is None:
                            await asyncio.sleep(1)
                    try:
                        await handle_multiprocessing_message(message)
                    except Exception:
                        LOGGER.exception("Error handling multiprocessing message")
            await asyncio.sleep(1)


## entrypoint


def main():
    """Entrypoint which runs bot"""
    # initiate multiprocessing link
    CLIENT.loop.create_task(multiprocessing_link())

    LOGGER.info("Connecting to discord")
    CLIENT.run(SECRETS["discord_token"])


## bot commands array

COMMANDS = dict(
    help=dict(function=help_, description="You just called it"),
    my_submissions=dict(
        function=my_submissions, description="Shows all your submissions"
    ),
    rules=dict(function=show_rules, description="Shows you all the rules"),
    force_approve=dict(
        function=force_approve,
        description="Forces the approval of a submission, ignoring votes [Admin only]",
    ),
    force_delete=dict(
        function=force_delete,
        description="Forces the deletion of a submission, ignoring votes [Admin only]",
    ),
    confirm=dict(
        function=confirm,
        description="Confirms important commands "
        "(%force_approve/%force_delete) [Admin only]",
    ),
    pro_gamers=dict(function=pro_gamers, description="The chosen ones [Admin only]"),
)


if __name__ == "__main__":
    try:
        main()
    except BaseException:
        LOGGER.exception("Fatal error")
        raise
